/* global R */

(function() {
  'use strict';

  function Model(Event) {
    var answer_key = {
      Q1: 'Boykin Spaniel',
      Q2: 'Carolina Jessamine',
      Q3: 'Columbia'
    };

    function meanCorrect(answers) {
      var answer_counts = R.countBy(R.equals(true), answers),
        number_correct = answer_counts['true'] ? answer_counts['true'] : 0;

      return Math.round((number_correct / answers.length) * 100);
    }

    function categorize(results) {
      let correct = R.filter(function(x) {
        return R.nth(1, x) === true;
      }, results);
      let incorrect = R.filter(function(x) {
        return R.nth(1, x) === false;
      }, results);

      return [R.map(R.nth(0), correct), R.map(R.nth(0), incorrect)];
    }

    function analyzeResults(results) {
      var answers = R.map(R.nth(1), results),
          score = meanCorrect(answers),
          [correct, incorrect] = categorize(results),
          analysis = {
            score: score,
            correct: correct,
            incorrect: incorrect,
            total_questions: results.length
          };

      Event.emit('results-ready', analysis);
    }

    function checkAnswers(answers) {
      var results = R.map(function(x) {
        return [x.name, R.equals(x.value, answer_key[x.name])];
      }, answers);

      Event.emit('answers-checked', results);
    }

    function init() {
      Event.on('check-answers', checkAnswers);
      Event.on('answers-checked', analyzeResults);
    }

    return {
      init: init
    };
  }

  module.exports = Model;

}());
