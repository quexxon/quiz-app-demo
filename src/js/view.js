/* eslint-env jquery */
/* global R, Quiz */

(function() {
  'use strict';

  var View = function(Event) {
    function markCorrect(q) {
      let el = $('input[name="' + q + '"]:checked');
      el.closest('.question').addClass('correct');
    }

    function markIncorrect(q) {
      let el = $('input[name="' + q + '"]:checked');
      el.closest('.question').addClass('incorrect');
    }

    function renderResults(results) {
      var context = {
        score: results.score,
        correct: results.correct.length,
        total: results.total_questions
      };
      R.forEach(markCorrect, results.correct);
      R.forEach(markIncorrect, results.incorrect);
      $('#results').html(Quiz.templates.score(context));
      $('#results > p').velocity({
        width: '100%',
        fontSize: '2rem',
        backgroundColor: '#F8F744',
        color: '#000'
      }, {
        duration: 400
      });
      $('button').hide();

      Event.emit('graded');
    }

    function disableInputs() {
      $('body').on('click', 'input, label', function(e) {
        e.preventDefault();
      });
      $('label').css({'cursor': 'default'});
    }

    function init() {
      $('form').submit(function(e) {
        Event.emit('check-answers', $(this).serializeArray());
        e.preventDefault();
      });

      Event.on('results-ready', renderResults);

      Event.on('graded', disableInputs);
    }

    return {
      init: init
    };
  };

  module.exports = View;
}());
